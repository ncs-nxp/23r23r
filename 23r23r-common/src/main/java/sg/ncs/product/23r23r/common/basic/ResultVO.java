package sg.ncs.product.23r23r.common.basic;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel(value = "ResultVO")
public class ResultVO <T> {

	@ApiModelProperty(notes = "Return code, 100 means success, non-100 means failure")
	private Integer code;

	@ApiModelProperty(notes = "Return message, success is \"success\", failure is specific failure information")
	private String message;

	@ApiModelProperty(notes = "Return data")
	private T data;



}

