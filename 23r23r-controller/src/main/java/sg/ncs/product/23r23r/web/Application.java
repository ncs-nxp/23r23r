package sg.ncs.product.23r23r.web.web;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@EnableFeignClients("sg.ncs.product.23r23r.web.*")
@SpringBootApplication(scanBasePackages = "sg.ncs.product.23r23r.web.*")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


}
